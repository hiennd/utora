﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="bread-crumbs.ascx.cs" Inherits="ustora.layouts.ustora.modules.bread_crumbs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2>Example</h2>
                </div>
            </div>
        </div>
    </div>
</div>