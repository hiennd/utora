﻿<%@ Page language="c#" Codepage="65001" AutoEventWireup="true" Inherits="ustora.layouts.ustora.Home" CodeBehind="Home.aspx.cs" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>


<!DOCTYPE html>

<html lang="en">
    <!-- Site <head> -->
    <sc:Sublayout ID="Head" Path="/layouts/ustora/site/Head.ascx" runat="server" />

    <!-- Site <body> -->
    <sc:Sublayout ID="Body" Path="/layouts/ustora/site/Body.ascx" runat="server" />

    <!-- Site <script> -->
    <sc:Sublayout ID="Script" Path="/layouts/ustora/site/Script.ascx" runat="server" />
</html>
