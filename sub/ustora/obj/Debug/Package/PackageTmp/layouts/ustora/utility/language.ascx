﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="language.ascx.cs" Inherits="ustora.layouts.ustora.utility.language" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<asp:Repeater ID="rpLanguage" ItemType="Sitecore.Globalization.Language" runat="server">
    <HeaderTemplate>
        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value"> <%#: CurrentLanguage %> </span><b class="caret"></b></a>
        <ul class="dropdown-menu">
    </HeaderTemplate>
    <ItemTemplate>
        <li><a href="/<%#: Item.Name %>"><%#: Item.Title %></a></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>