﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Web.UI;
using Sitecore.Collections;
using System.Web.UI;
using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

namespace ustora.webcontrols
{
    public class Gallery : WebControl
    {
        private static readonly string galleryItemImage = "Image";
        private static readonly string galleryItemProgram = "Program";
        private static readonly string galleryItemStore = "Store";
        private static readonly string galleryItemUnit = "Unit";
        private static readonly string galleryItemCaption = "Caption";
        private static readonly string galleryItemLink = "Link";
        /*
            <div class="slider-area">
		        <div class="block-slider">
			        <ul id="bxslider-home">
                        <li>
					        <img src = "/static/img/h4-slide.png" alt="Slide">
					        <div class="caption-group">
						        <h2 class="caption title">
							        iPhone
                                    <span class="primary">
                                        6
                                        <strong>Plus</strong>
                                    </span>
						        </h2>
						        <h4 class="caption subtitle">Dual SIM</h4>
						        <a class="caption button-radius" href="#"><span class="icon"></span>Shop now</a>
					        </div>
				        </li>
			        </ul>
		        </div>
            </div>
        */
        protected override void DoRender(HtmlTextWriter output)
        {
            Item galleryItem = this.GetItem();

            // <div class="slider-area">
            output.AddAttribute(HtmlTextWriterAttribute.Class, "slider-area");
            output.RenderBeginTag(HtmlTextWriterTag.Div);

                // <div class="block-slider">
                output.AddAttribute(HtmlTextWriterAttribute.Class, "block-slider block-slider4");
                output.RenderBeginTag(HtmlTextWriterTag.Div);

                    output.AddAttribute(HtmlTextWriterAttribute.Id, "bxslider-home4");
                    output.RenderBeginTag(HtmlTextWriterTag.Ul);

                        for (int i = 0; i < galleryItem.GetChildren().Count; i++)
                        {
                            output.RenderBeginTag(HtmlTextWriterTag.Li);
                                output.Write(FieldRenderer.Render(galleryItem.Children[i], galleryItemImage));
                            

                                // <div class="caption-group">
                                output.AddAttribute(HtmlTextWriterAttribute.Class, "caption-group");
                                output.RenderBeginTag(HtmlTextWriterTag.Div);

                                    //<h2 class="caption title">
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "caption title");
                                    output.RenderBeginTag(HtmlTextWriterTag.H2);
                                        output.Write(FieldRenderer.Render(galleryItem.Children[i], galleryItemProgram));
                                        output.AddAttribute(HtmlTextWriterAttribute.Class, "primary");
                                        output.RenderBeginTag(HtmlTextWriterTag.Span);
                                            output.Write("&nbsp;");
                                            output.Write(FieldRenderer.Render(galleryItem.Children[i], galleryItemStore));
                                            output.RenderBeginTag(HtmlTextWriterTag.Strong);
                                                output.Write("&nbsp;");
                                                output.Write(FieldRenderer.Render(galleryItem.Children[i], galleryItemUnit));
                                            output.RenderEndTag();
                                        output.RenderEndTag();
                                    output.RenderEndTag();

                                    //<h4 class="caption subtitle">
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "caption subtitle");
                                    output.RenderBeginTag(HtmlTextWriterTag.H4);
                                        output.Write(FieldRenderer.Render(galleryItem.Children[i], galleryItemCaption));
                                    output.RenderEndTag();

                                    //<a class="caption button-radius" href="#">
                                    output.AddAttribute(HtmlTextWriterAttribute.Class, "caption button-radius");
                                    output.AddAttribute(HtmlTextWriterAttribute.Href, FieldRenderer.Render(galleryItem.Children[i], galleryItemLink));
                                    output.RenderBeginTag(HtmlTextWriterTag.A);
                                        output.AddAttribute(HtmlTextWriterAttribute.Class, "icon");
                                        output.RenderBeginTag(HtmlTextWriterTag.Span);
                                        output.RenderEndTag();
                                        output.Write("Shop now");
                                    output.RenderEndTag();

                                output.RenderEndTag();
                            output.RenderEndTag();
                        }

                    output.RenderEndTag();

                output.RenderEndTag();

            output.RenderEndTag();
        }
    }
}