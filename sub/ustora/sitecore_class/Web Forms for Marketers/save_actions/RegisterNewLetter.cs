﻿using Sitecore.Data;
using Sitecore.WFFM.Abstractions.Actions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using System;
using System.Web;
using Sitecore.SecurityModel;
using Sitecore.Configuration;
using ustora.QueryLinq;
using System.Linq;

namespace CustomWFFM.CustomSaveActions
{
    public class RegisterNewLetter : ISaveAction
    {
        public void Execute(ID formId, AdaptedResultList adaptedFields, ActionCallContext actionCallContext = null, params object[] data)
        {
            try
            {
                // Get Field Value
                AdaptedControlResult email = adaptedFields.GetEntry(this.Email, "Email");
                string EmailValue = email.Value;

                // Get Item
                Database master = Factory.GetDatabase("master");
                var ParentItem = master.GetItem("{BD4DDC19-DAE8-44F3-B46F-391BDAF2AA9B}"); //User Get Letter
                var ItemTemplate = master.GetTemplate("{5003C310-7634-472F-952B-EEC71D9E4CC7}"); // User Subcrible

                // Search for check conflict
                var index = ContentSearchManager.GetIndex("sitecore_master_index");
                var defaultLang = "en";
                var contextLang = Sitecore.Context.Language != null ? Sitecore.Context.Language.Name : defaultLang;


                using (var context = index.CreateSearchContext())
                {
                    var queryable = context.GetQueryable<QueryUstora>()
                        .Where(x => x.TemplateName.Equals("User Subcrible") && (x.Language == contextLang) && (x.Email == EmailValue));

                    var results = queryable.GetResults();

                    if (results.TotalSearchResults == 0)
                    {
                        using (new SecurityDisabler())
                        {
                            var NewName = HttpUtility.HtmlEncode(EmailValue.Split('@', '.')[0]) + "_" + Sitecore.DateUtil.IsoNow;
                            var NewItem = ParentItem.Add(NewName, ItemTemplate);
                            try
                            {
                                if (NewItem != null)
                                {
                                    NewItem.Editing.BeginEdit();
                                    NewItem["Email"] = HttpUtility.HtmlEncode(EmailValue);
                                    NewItem.Editing.EndEdit();
                                }
                            }
                            catch
                            {
                                NewItem.Editing.CancelEdit();
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Conflict record !");
                    }
                }
            }
            catch (Exception) {
                throw new Exception("There are somethings wrong when add email !");
            }
        }

        public ActionState QueryState(ActionQueryContext queryContext)
        {
            return ActionState.Enabled;
        }
        
        public string Email { get; set; }
        public ID ActionID { get; set; }
        public string UniqueKey { get; set; }
        public ActionType ActionType { get; set; }
    }
}