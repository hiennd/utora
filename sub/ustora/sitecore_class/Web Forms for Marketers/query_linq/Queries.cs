﻿using System;
using System.Collections.Generic;
using System.Web;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.ContentSearch;

namespace ustora.QueryLinq
{
    public class QueryUstora : SearchResultItem
    {
        [IndexField("email")]
        public string Email { get; set; }
    }
}