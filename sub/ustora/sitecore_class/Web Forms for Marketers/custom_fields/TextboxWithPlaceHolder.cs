﻿using Sitecore.Form.Core.Attributes;
using Sitecore.Form.Web.UI.Controls;
using System;
using System.ComponentModel;
using System.Web.UI;
namespace CustomWFFM.CustomFieldTypes
{
    [ValidationProperty("Text")]
    public class EmailWithPlaceholderField : Email
    {
        [VisualCategory("Custom Properties")]
        [VisualProperty("Placeholder Text", 2)]
        [DefaultValue("Type your email")]
        public string PlaceholderText { get; set; }

        protected override void OnInit(EventArgs e)
        {
            if (!string.IsNullOrEmpty(PlaceholderText))
            {
                textbox.Attributes["placeholder"] = PlaceholderText;
            }

            base.OnInit(e);
        }
    }
}