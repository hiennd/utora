﻿namespace ustora.layouts.ustora.utility
{
    using System;
    using Sitecore.Collections;
    using Sitecore.Data.Managers;
    using Sitecore.Configuration;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Globalization;

    public partial class language : System.Web.UI.UserControl
    {
        public String CurrentLanguage { get; set; }
        private void Page_Load(object sender, EventArgs e)
        {
            Database master = Factory.GetDatabase("master");
            LanguageCollection languageCollection = LanguageManager.GetLanguages(master);

            ID contextLanguageId = LanguageManager.GetLanguageItemId(Sitecore.Context.Language, master);
            Item contextLanguage = master.GetItem(contextLanguageId);
            CurrentLanguage = contextLanguage.Fields["Code page"].ToString();
            if (languageCollection.Count > 0)
            {
                rpLanguage.DataSource = languageCollection;
                rpLanguage.DataBind();
            }
        }
    }
}