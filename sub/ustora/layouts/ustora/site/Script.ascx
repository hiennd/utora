﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Script.ascx.cs" Inherits="ustora.layouts.ustora.site.Script" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

   
<!-- Latest jQuery form server -->
<script src="/static/js/jquery.min.js"></script>
    
<!-- Bootstrap JS form CDN -->
<script src="/static/js/bootstrap.min.js"></script>
    
<!-- jQuery sticky menu -->
<script src="/static/js/owl.carousel.min.js"></script>
<script src="/static/js/jquery.sticky.js"></script>
    
<!-- jQuery easing -->
<script src="/static/js/jquery.easing.1.3.min.js"></script>
    
<!-- Main Script -->
<script src="/static/js/main.js"></script>
    
<!-- Slider -->
<script type="text/javascript" src="/static/js/bxslider.min.js"></script>
<script type="text/javascript" src="/static/js/script.slider.js"></script>