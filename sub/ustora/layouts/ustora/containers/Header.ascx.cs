﻿namespace ustora.layouts.ustora.containers
{
    using System;
    using Sitecore;
    using Sitecore.Data.Items;

    public partial class Header : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            scLogo.Item = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);
        }
    }
}