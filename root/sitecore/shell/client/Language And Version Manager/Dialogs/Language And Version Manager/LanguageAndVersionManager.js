﻿define(["sitecore"], function (Sitecore) {

    var CustomWorkflowManager = Sitecore.Definitions.App.extend({

        filesUploaded: [],

        initialized: function () {
            this.GetLanguages();
        },



        GetLanguages: function () {
            $.ajax({
                url: "/api/sitecore/LanguageAndVersionManager/GetLanguages",
                type: "POST",
                data: {  },
                context: this,
                success: function (data) {
                    var json = jQuery.parseJSON(data);
                    this.workflow.set("items", json);
                    this.ComboBoxCopyFrom.set("items", json)

                }
            });
        },


        AddVersion: function () {
            this.pi.viewModel.show();
            this.miMessages.viewModel.notifications.removeAll();
            this.miMessages.viewModel.warnings.removeAll();
            var selectedLanguage = this.workflow.viewModel.selectedValue();
            var selectedTemplates = this.treeViewTemplates.viewModel.checkedItemIds();
            var rootItem = this.txtRootPath.get("text");
            $.ajax({
                url: "/api/sitecore/LanguageAndVersionManager/AddVersions",
                type: "POST",
                data: { language: selectedLanguage, "selectedTemplates": selectedTemplates, "rootItem": rootItem },
                context: this,
                success: function (data) {
                    if (data.indexOf("NotValidItem") >= 0) {
                        this.miMessages.viewModel.notifications.removeAll();
                        this.miMessages.addMessage("warning", { text: "Value entered in RootItem field is incorrect Please try again", actions: [], closable: true, temporary: true });
                    } else {
                        this.miMessages.viewModel.notifications.removeAll();
                        var itemReportJson = jQuery.parseJSON(data);
                        if (itemReportJson.length <= 0) {
                            this.miMessages.addMessage("warning", { text: "No items updated as all the processed items already having version with selected language", actions: [], closable: true, temporary: true });
                        }
                        else if (data.indexOf("false") >= 0) {
                            this.miMessages.viewModel.notifications.removeAll();
                            this.miMessages.addMessage("warning", { text: "Something is wrong please try again", actions: [], closable: true, temporary: true });
                        }
                        else {
                           // this.itemReportListing.set("items", itemReportJson);
                            this.miMessages.addMessage("notification", { text: "Version added successfully on selected Items", actions: [], closable: true, temporary: true });
                            //this.UpdatedItemTitle.viewModel.show();
                            //this.itemReportListing.viewModel.show();
                        }
                    } 
                    this.pi.viewModel.hide();
                }
            });
        },



        CopyContent: function () {
            this.pi.viewModel.show();
            this.miMessages.viewModel.notifications.removeAll();
            this.miMessages.viewModel.warnings.removeAll();
            var selectedLanguageCopyTo = this.workflow.viewModel.selectedValue();
            var copyFromCombo = this.ComboBoxCopyFrom.viewModel.selectedValue();
            var selectedTemplates = this.treeViewTemplates.viewModel.checkedItemIds();
            var rootItem = this.txtRootPath.get("text");

            $.ajax({
                url: "/api/sitecore/LanguageAndVersionManager/CopyContent",
                type: "POST",
                data: { srcLang: copyFromCombo, targetLang: selectedLanguageCopyTo, "selectedTemplates": selectedTemplates, "rootItem": rootItem },
                context: this,
                success: function (data) {
                    if (data.indexOf("NotValidItem") >= 0) {
                        this.miMessages.viewModel.notifications.removeAll();
                        this.miMessages.addMessage("warning", { text: "Value entered in RootItem field is incorrect Please try again", actions: [], closable: true, temporary: true });
                    } else if (data.indexOf("NoResult") >= 0) {
                        this.miMessages.viewModel.notifications.removeAll();
                        this.miMessages.addMessage("warning", { text: "Sitecore Query enterd in Root Item field doesn't return any items to update Please check query again", actions: [], closable: true, temporary: true });
                    } else if (data.indexOf("false") >= 0) {
                        this.miMessages.viewModel.notifications.removeAll();
                        this.miMessages.addMessage("warning", { text: "Something is wrong please try again", actions: [], closable: true, temporary: true });
                    }

                    else {
                        this.miMessages.viewModel.notifications.removeAll();
                        var itemReportJson = jQuery.parseJSON(data);
                        if (itemReportJson.length <= 0) {
                            this.miMessages.addMessage("warning", { text: "No items updated as all the processed items already having content with selected language", actions: [], closable: true, temporary: true });
                        }
                        else {
                            //this.itemReportListing.set("items", itemReportJson);
                            this.miMessages.addMessage("notification", { text: "Content copied successfully on selected Items", actions: [], closable: true, temporary: true });
                            //this.UpdatedItemTitle.viewModel.show();
                            //this.itemReportListing.viewModel.show();
                        }
                    }
                    this.pi.viewModel.hide();
                }
            });
        },

    });

    return CustomWorkflowManager;
});