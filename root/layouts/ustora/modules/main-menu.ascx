﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="main-menu.ascx.cs" Inherits="ustora.layouts.ustora.modules.main_menu" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
   
<div class="mainmenu-area">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div> 
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <sc:Placeholder runat="server" ID="scMenu" Key="Menu"/>
                </ul>
            </div>  
        </div>
    </div>
</div> <!-- End mainmenu area -->
