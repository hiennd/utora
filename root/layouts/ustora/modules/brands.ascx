﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="brands.ascx.cs" Inherits="ustora.layouts.ustora.modules.brands" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="brands-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="brand-wrapper">
                    <div class="brand-list">
                        <img src="/static/img/brand1.png" alt="">
                        <img src="/static/img/brand2.png" alt="">
                        <img src="/static/img/brand3.png" alt="">
                        <img src="/static/img/brand4.png" alt="">
                        <img src="/static/img/brand5.png" alt="">
                        <img src="/static/img/brand6.png" alt="">
                        <img src="/static/img/brand1.png" alt="">
                        <img src="/static/img/brand2.png" alt="">                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End brands area -->