﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="New letter.ascx.cs" Inherits="ustora.layouts.ustora.webforms.New_letter" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpleForm.cs" Inherits="Sitecore.Form.Core.Ascx.Controls.SimpleForm" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI.WebControls" Assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>
<%@ Register TagPrefix="cc0" Namespace="Sitecore.Form.Web.UI.Controls" Assembly="Sitecore.Forms.Core" %>
<%@ Register TagPrefix="cc1" Namespace="System.Web.UI" Assembly="System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>
<%@ Register TagPrefix="cc2" Namespace="Sitecore.Form.Core.Controls.Html" Assembly="Sitecore.Forms.Core" %>
<%@ Register TagPrefix="cc4" Namespace="Sitecore.Form.Validators" Assembly="Sitecore.Forms.Core" %>
<%@ Register TagPrefix="cc5" Namespace="Sitecore.Form.Core.Controls" Assembly="Sitecore.Forms.Core" %>
<%@ Register TagPrefix="cc3" Namespace="Sitecore.Form.Web.UI.Controls" Assembly="Sitecore.Forms.Custom" %>
<%@ Register TagPrefix="cc6" Namespace="Sitecore.Form.Core.Validators" Assembly="Sitecore.Forms.Core" %>
<link type="text/css" rel="stylesheet" href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/Default.css?v=17072012"/>
<link type="text/css" rel="stylesheet" href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/colors//jquery-ui.custom.Default.css"/>
<link type="text/css" rel="stylesheet" href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/colors/Default.css"/>
<link type="text/css" rel="stylesheet" href="/sitecore%20modules/shell/Web%20Forms%20for%20Marketers/themes/Custom.css"/>
<asp:hiddenfield runat="server" value="form_518CA52372554A5B94D26CE103089DAA" id="formreference">
</asp:hiddenfield>
aaa
<h1 runat="server" class="scfTitleBorder" id="title">
  <asp:literal runat="server" text="Newsletter">
  </asp:literal>
</h1>
<div runat="server" class="scfIntroBorder" id="intro">
</div>
<asp:validationsummary runat="server" id="_summary" forecolor="" validationgroup="form_518CA52372554A5B94D26CE103089DAA_submit" cssclass="scfValidationSummary">
</asp:validationsummary>
<cc0:submitsummary runat="server" id="form_518CA52372554A5B94D26CE103089DAA_submitSummary" cssclass="scfSubmitSummary" >
</cc0:submitsummary>
<asp:panel runat="server" id="fieldContainer" defaultbutton="form_518CA52372554A5B94D26CE103089DAA_submit">
  <div runat="server" disablewebediting="False" renderingparameters="FormID={518CA523-7255-4A5B-94D2-6CE103089DAA}">
    <div runat="server" class="scfSectionBorder">
      <div runat="server" class="scfSectionContent">
        <cc3:email runat="server" cssclass="scfEmailBorder fieldid.%7b4D61A5A6-9138-478C-8742-1E79209C41C2%7d name.Email" maxlength="0" title="Email" id="field_4D61A5A69138478C87421E79209C41C2" result="Sitecore.WFFM.Abstractions.Actions.ControlResult" controlname="Email" fieldid="{4D61A5A6-9138-478C-8742-1E79209C41C2}" >
          <validators>
            <asp:regularexpressionvalidator runat="server" errormessage="Email contains an invalid address." validationexpression="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" forecolor="" controltovalidate="field_4D61A5A69138478C87421E79209C41C2" display="Dynamic" validationgroup="form_518CA52372554A5B94D26CE103089DAA_submit" cssclass="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b4D61A5A6-9138-478C-8742-1E79209C41C2%7d inner.1" id="field_4D61A5A69138478C87421E79209C41C25D10AF7533054C39908EB25E8CB4ABDC_validator">Enter a valid email address.</asp:regularexpressionvalidator>
            <cc4:requiredwithmarkervalidator runat="server" forecolor="" controltovalidate="field_4D61A5A69138478C87421E79209C41C2" errormessage="Email must be filled in.&#xD;&#xA;" enableclientscript="False" display="Dynamic" validationgroup="form_518CA52372554A5B94D26CE103089DAA_submit" cssclass="scfValidatorRequired trackevent.%7b7E86B2F5-ACEC-4C60-8922-4EB5AE5D9874%7d fieldid.%7b4D61A5A6-9138-478C-8742-1E79209C41C2%7d" tooltip="Email must be filled out." id="field_4D61A5A69138478C87421E79209C41C28937C6DD55554D86B126816635E96167_validator" >*</cc4:requiredwithmarkervalidator>
          </validators>
          <requred>
            <cc5:markerlabel runat="server" cssclass="scfRequired" >*</cc5:markerlabel>
          </requred>
        </cc3:email>
        <cc3:singlelinetext runat="server" cssclass="scfSingleLineTextBorder fieldid.%7b3F99837A-F4E3-4624-B090-EE3AAA51AA03%7d name.Description" predefinedvalidatortextmessage="" title="Description" id="field_3F99837AF4E34624B090EE3AAA51AA03" result="Sitecore.WFFM.Abstractions.Actions.ControlResult" controlname="Description" fieldid="{3F99837A-F4E3-4624-B090-EE3AAA51AA03}" >
          <validators>
            <cc6:countcharsvalidator runat="server" maxlength="256" minlength="0" validationexpression="(.|\n){0,256}$" forecolor="" controltovalidate="field_3F99837AF4E34624B090EE3AAA51AA03" errormessage="Description must have at least 0 and no more than 256 characters." display="Dynamic" validationgroup="form_518CA52372554A5B94D26CE103089DAA_submit" cssclass="scfValidator trackevent.%7bF3D7B20C-675C-4707-84CC-5E5B4481B0EE%7d fieldid.%7b3F99837A-F4E3-4624-B090-EE3AAA51AA03%7d inner.1" id="field_3F99837AF4E34624B090EE3AAA51AA036ADFFAE3DADB451AB530D89A2FD0307B_validator" >Description must have at least 0 and no more than 256 characters.</cc6:countcharsvalidator>
            <cc6:customregularexpressionvalidator runat="server" predefinedvalidatortextmessage="The value of the Description field is not valid." validationexpression="^(.|\n)*$" forecolor="" controltovalidate="field_3F99837AF4E34624B090EE3AAA51AA03" errormessage="The value of the Description field is not valid." display="Dynamic" validationgroup="form_518CA52372554A5B94D26CE103089DAA_submit" cssclass="scfValidator trackevent.%7b844BBD40-91F6-42CE-8823-5EA4D089ECA2%7d fieldid.%7b3F99837A-F4E3-4624-B090-EE3AAA51AA03%7d inner.1" id="field_3F99837AF4E34624B090EE3AAA51AA03070FCA141E9A45D78611EA650F20FE77_validator" >The value of the Description field is not valid.</cc6:customregularexpressionvalidator>
          </validators>
          <requred>
          </requred>
        </cc3:singlelinetext>
      </div>
    </div>
  </div>
</asp:panel>
<div runat="server" class="scfFooterBorder" id="footer">
</div>
<div runat="server" class="scfSubmitButtonBorder">
  <cc0:submitbutton runat="server" onclientclick="$scw.webform.lastSubmit = this.id;" text="Subscribe" validationgroup="form_518CA52372554A5B94D26CE103089DAA_submit" cssclass="scfSubmitButton" id="form_518CA52372554A5B94D26CE103089DAA_submit" onclick="OnClick" >
  </cc0:submitbutton>
</div>
<asp:hiddenfield runat="server" value="1" id="form_518CA52372554A5B94D26CE103089DAA_eventcount">
</asp:hiddenfield>
<asp:hiddenfield runat="server" value="33b79abd-45e2-41d7-9aef-c255a543f619" id="form_518CA52372554A5B94D26CE103089DAA_anticsrf">
</asp:hiddenfield>
<asp:hiddenfield runat="server" value="Thank you for filling in the form." id="form_518CA52372554A5B94D26CE103089DAA_successmessage">
</asp:hiddenfield>