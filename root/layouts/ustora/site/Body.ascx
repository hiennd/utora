﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Body.ascx.cs" Inherits="ustora.layouts.ustora.site.Body" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<body>
    <sc:Placeholder runat="server" ID="phMain" Key="Main"/>
</body>